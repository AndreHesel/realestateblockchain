/*
* Use this file for functional testing of your smart contract.
* Fill out the arguments and return values for a function and
* use the CodeLens links above the transaction blocks to
* invoke/submit transactions.
* All transactions defined in your smart contract are used here
* to generate tests, including those functions that would
* normally only be used on instantiate and upgrade operations.
* This basic test file can also be used as the basis for building
* further functional tests to run as part of a continuous
* integration pipeline, or for debugging locally deployed smart
* contracts by invoking/submitting individual transactions.
*/
/*
* Generating this test file will also trigger an npm install
* in the smart contract project directory. This installs any
* package dependencies, including fabric-network, which are
* required for this test file to be run locally.
*/

import * as assert from 'assert';
import * as fabricNetwork from 'fabric-network';
import { SmartContractUtil } from './ts-smart-contract-util';

import * as os from 'os';
import * as path from 'path';

describe('RealEstateAssetContract-RealEstateContract@0.0.2' , () => {

    const homedir: string = os.homedir();
    const walletPath: string = path.join(homedir, '.fabric-vscode', 'v2', 'environments', '1 Org Local Fabric', 'wallets', 'Org1');
    const gateway: fabricNetwork.Gateway = new fabricNetwork.Gateway();
    let fabricWallet: fabricNetwork.Wallet;
    const identityName: string = 'Org1 Admin';
    let connectionProfile: any;

    before(async () => {
        connectionProfile = await SmartContractUtil.getConnectionProfile();
        fabricWallet = await fabricNetwork.Wallets.newFileSystemWallet(walletPath);
    });

    beforeEach(async () => {
        const discoveryAsLocalhost: boolean = SmartContractUtil.hasLocalhostURLs(connectionProfile);
        const discoveryEnabled: boolean = true;

        const options: fabricNetwork.GatewayOptions = {
            discovery: {
                asLocalhost: discoveryAsLocalhost,
                enabled: discoveryEnabled,
            },
            identity: identityName,
            wallet: fabricWallet,
        };

        await gateway.connect(connectionProfile, options);
    });

    afterEach(async () => {
        gateway.disconnect();
    });


    describe('createRealEstateAsset', () => {
        it('should submit createRealEstateAsset transaction', async () => {
            // TODO: populate transaction parameters
            const realEstateAssetId: string = '005';
            const type: string = 'LANDED HOUSE';
            const address: string = 'JL. Kali 11';
            const postalCode: number = 211;
            const buildingArea: number = 20;
            const surfaceArea: number = 10;
            const price: number = 1000000;
            const args: string[] = [ realEstateAssetId, type, address, postalCode.toString(), buildingArea.toString(), surfaceArea.toString(), price.toString()];
            const response: Buffer = await SmartContractUtil.submitTransaction('RealEstateAssetContract', 'createRealEstateAsset', args, gateway);
            
            // submitTransaction returns buffer of transcation return value
            // TODO: Update with return value of transaction
            assert.strictEqual(true, true);
            // assert.strictEqual(JSON.parse(response.toString()), undefined);
        }).timeout(10000);
    });

    describe('realEstateAssetExists', () => {
        it('should evaluate realEstateAssetExists transaction', async () => {
            // TODO: populate transaction parameters
            const realEstateAssetId: string = '005';
            const args: string[] = [ realEstateAssetId];
            const response: Buffer = await SmartContractUtil.evaluateTransaction('RealEstateAssetContract', 'realEstateAssetExists', args, gateway);
            
            // submitTransaction returns buffer of transcation return value
            // TODO: Update with return value of transaction
            // assert.strictEqual(true, true);
            assert.strictEqual(JSON.parse(response.toString()), true);
        }).timeout(10000);
    });

    describe('readRealEstateAsset', () => {
        it('should evaluate readRealEstateAsset transaction', async () => {
            // TODO: populate transaction parameters
            const realEstateAssetId: string = '005';
            const args: string[] = [ realEstateAssetId];
            const response: Buffer = await SmartContractUtil.evaluateTransaction('RealEstateAssetContract', 'readRealEstateAsset', args, gateway);
            const obj = {
                type : 'LANDED HOUSE',
                address : 'JL. Kali 11',
                postalCode : 211,
                buildingArea : 20,
                surfaceArea : 10,
                price: 1000000
            };
            // submitTransaction returns buffer of transcation return value
            // TODO: Update with return value of transaction
            // assert.strictEqual(true, true);
            assert.strictEqual(JSON.parse(response.toString()), obj);
        }).timeout(10000);
    });

    describe('updateRealEstateAsset', () => {
        it('should submit updateRealEstateAsset transaction', async () => {
            // TODO: populate transaction parameters
            const realEstateAssetId: string = '005';
            const buildingArea: number = 74;
            const surfaceArea: number = 48;
            const price: number = 9900000000;
            const args: string[] = [ realEstateAssetId, buildingArea.toString(), surfaceArea.toString(), price.toString()];
            const response: Buffer = await SmartContractUtil.submitTransaction('RealEstateAssetContract', 'updateRealEstateAsset', args, gateway);
            
            // submitTransaction returns buffer of transcation return value
            // TODO: Update with return value of transaction
             assert.strictEqual(true, true);
            // assert.strictEqual(JSON.parse(response.toString()), true);
        }).timeout(10000);
    });

    describe('queryAllAssets', () => {
        it('should submit queryAllAssets transaction', async () => {
            // TODO: Update with parameters of transaction
            const args: string[] = [];
            const response: Buffer = await SmartContractUtil.submitTransaction('RealEstateAssetContract', 'queryAllAssets', args, gateway);
            
            // submitTransaction returns buffer of transcation return value
            // TODO: Update with return value of transaction
            //assert.strictEqual(true, true);
            assert.deepStrictEqual(JSON.parse(response.toString()), [{
                Key: '005',
                Record: {
                    address: 'BSD City Tabebuya. C8/8',
                    buildingArea: 74,
                    postalCode: 211,
                    price: 9900000000,
                    surfaceArea: 48,
                    type: 'LANDED HOUSE'
                }
            }]);
        }).timeout(10000);
    });

    describe('queryByType', () => {
        it('should submit queryByType transaction', async () => {
            // TODO: populate transaction parameters
            const type: string = 'EXAMPLE';
            const args: string[] = [ type];
            const response: Buffer = await SmartContractUtil.submitTransaction('RealEstateAssetContract', 'queryByType', args, gateway);
            
            // submitTransaction returns buffer of transcation return value
            // TODO: Update with return value of transaction
            assert.strictEqual(true, true);
            // assert.strictEqual(JSON.parse(response.toString()), undefined);
        }).timeout(10000);
    });

    describe('queryByPriceRange', () => {
        it('should submit queryByPriceRange transaction', async () => {
            // TODO: populate transaction parameters
            const minPrice: number = 9900000000;
            const maxPrice: number = 0;
            const size: number = 5;
            const bookmark: string = '';
            const args: string[] = [ minPrice.toString(), maxPrice.toString(), size.toString(), bookmark];
            const response: Buffer = await SmartContractUtil.submitTransaction('RealEstateAssetContract', 'queryByPriceRange', args, gateway);
            
            // submitTransaction returns buffer of transcation return value
            // TODO: Update with return value of transaction
            assert.deepStrictEqual(JSON.parse(response.toString()), [{
                Key: '005',
                Record: {
                    address: 'BSD City Tabebuya. C8/8',
                    buildingArea: 74,
                    postalCode: 211,
                    price: 9900000000,
                    surfaceArea: 48,
                    type: 'LANDED HOUSE'
                }
            }]);
        }).timeout(10000);
    });

    describe('queryByPriceRange', () => {
        it('should submit queryByPriceRange transaction', async () => {
            // TODO: populate transaction parameters
            const minPrice: number = 999900000000;
            const maxPrice: number = 999900000000;
            const size: number = 5;
            const bookmark: string = '';
            const args: string[] = [ minPrice.toString(), maxPrice.toString(), size.toString(), bookmark];
            const response: Buffer = await SmartContractUtil.submitTransaction('RealEstateAssetContract', 'queryByPriceRange', args, gateway);
            
            // submitTransaction returns buffer of transcation return value
            // TODO: Update with return value of transaction
            assert.deepStrictEqual(JSON.parse(response.toString()), []);
        }).timeout(10000);
    });


    // AR Migrating this to the end as best practice 
    describe('deleteRealEstateAsset', () => {
        it('should submit deleteRealEstateAsset transaction', async () => {
            // TODO: populate transaction parameters
            const realEstateAssetId: string = 'EXAMPLE';
            const args: string[] = [ realEstateAssetId];
            const response: Buffer = await SmartContractUtil.submitTransaction('RealEstateAssetContract', 'deleteRealEstateAsset', args, gateway);
            
            // submitTransaction returns buffer of transcation return value
            // TODO: Update with return value of transaction
            // assert.strictEqual(true, true);
            assert.strictEqual(JSON.parse(response.toString()), undefined);
        }).timeout(10000);
    });
});
