/*
 * SPDX-License-Identifier: Apache-2.0
 */

import { RealEstateAssetContract } from './real-estate-asset-contract';
export { RealEstateAssetContract } from './real-estate-asset-contract';

export const contracts: any[] = [ RealEstateAssetContract ];
