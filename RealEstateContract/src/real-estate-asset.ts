/*
 * SPDX-License-Identifier: Apache-2.0
 */

import { Object, Property } from 'fabric-contract-api';

@Object()
export class RealEstateAsset {

    @Property()
    //public value: string;
    public type : string;
    public address: string;
    public postalCode : number;

    // AR: Updatable data
    public buildingArea : number;
    public surfaceArea : number;
    public price: number;

}
