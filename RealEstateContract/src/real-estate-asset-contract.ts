/*
 * SPDX-License-Identifier: Apache-2.0
 */

import { Context, Contract, Info, Returns, Transaction } from 'fabric-contract-api';
import { RealEstateAsset } from './real-estate-asset';

@Info({title: 'RealEstateAssetContract', description: 'My Smart Contract' })
export class RealEstateAssetContract extends Contract {

    @Transaction(false)
    @Returns('boolean')
    public async realEstateAssetExists(ctx: Context, realEstateAssetId: string): Promise<boolean> {
        const data: Uint8Array = await ctx.stub.getState(realEstateAssetId);
        return (!!data && data.length > 0);
    }

    @Transaction()
    public async createRealEstateAsset(ctx: Context, realEstateAssetId: string, 
                                        type : string, address: string, postalCode : number,
                                        buildingArea : number, surfaceArea : number, price: number): Promise<void> {

        const hasAccess = await this.hasRole(ctx, ['Goverment']);
        if (!hasAccess) {
            throw new Error(`Only Goverment can create realestate asset`);
        }
                                    
        const exists: boolean = await this.realEstateAssetExists(ctx, realEstateAssetId);
        if (exists) {
            throw new Error(`The real estate asset ${realEstateAssetId} already exists`);
        }
        const realEstateAsset: RealEstateAsset = new RealEstateAsset();
        realEstateAsset.type = type;
        realEstateAsset.address = address;
        realEstateAsset.postalCode = postalCode;

        realEstateAsset.buildingArea = buildingArea;
        realEstateAsset.surfaceArea = surfaceArea;
        realEstateAsset.price = price;

        const buffer: Buffer = Buffer.from(JSON.stringify(realEstateAsset));
        await ctx.stub.putState(realEstateAssetId, buffer);
    }

    @Transaction(false)
    @Returns('RealEstateAsset')
    public async readRealEstateAsset(ctx: Context, realEstateAssetId: string): Promise<RealEstateAsset> {
        const exists: boolean = await this.realEstateAssetExists(ctx, realEstateAssetId);
        if (!exists) {
            throw new Error(`The real estate asset ${realEstateAssetId} does not exist`);
        }
        const data: Uint8Array = await ctx.stub.getState(realEstateAssetId);
        const realEstateAsset: RealEstateAsset = JSON.parse(data.toString()) as RealEstateAsset;
        return realEstateAsset;
    }

    @Transaction()
    public async updateRealEstateAsset(ctx: Context, realEstateAssetId: string, 
                                         buildingArea : number, surfaceArea : number, price: number): Promise<void> {
        
        const hasAccess = await this.hasRole(ctx, ['Goverment', 'Buyer']);
        if (!hasAccess) {
            throw new Error(`Only Goverment or Buyer can update car asset`);
        }
                                    
        const exists: boolean = await this.realEstateAssetExists(ctx, realEstateAssetId);
        if (!exists) {
            throw new Error(`The real estate asset ${realEstateAssetId} does not exist`);
        }
        const realEstateAsset: RealEstateAsset = new RealEstateAsset();
       
        realEstateAsset.buildingArea = buildingArea;
        realEstateAsset.surfaceArea = surfaceArea;
        realEstateAsset.price = price;
        const buffer: Buffer = Buffer.from(JSON.stringify(realEstateAsset));
        await ctx.stub.putState(realEstateAssetId, buffer);
    }

    @Transaction()
    public async deleteRealEstateAsset(ctx: Context, realEstateAssetId: string): Promise<void> {
        const hasAccess = await this.hasRole(ctx, ['Buyer']);
        if (!hasAccess) {
            throw new Error(`Only Buyer can create realestate asset`);
        }

        const exists: boolean = await this.realEstateAssetExists(ctx, realEstateAssetId);
        if (!exists) {
            throw new Error(`The real estate asset ${realEstateAssetId} does not exist`);
        }
        await ctx.stub.deleteState(realEstateAssetId);
    }

    @Transaction(false)
    public async queryAllAssets(ctx: Context): Promise<string> {
        const startKey = '000';
        const endKey = '999';
        const iterator = await ctx.stub.getStateByRange(startKey, endKey);
        const allResults = [];
        while (true) {
            const res = await iterator.next();
            if (res.value && res.value.value.toString()) {
                console.log(res.value.value.toString());

                const Key = res.value.key;
                let Record;
                try {
                    Record = JSON.parse(res.value.value.toString());
                } catch (err) {
                    console.log(err);
                    Record = res.value.value.toString();
                }
                allResults.push({ Key, Record });
            }
            if (res.done) {
                console.log('end of data');
                await iterator.close();
                console.info(allResults);
                return JSON.stringify(allResults);
            }
        }
    }

    @Transaction(false)
    public async queryByType(ctx: Context, type : string): Promise<string> {
        const query = { selector: {type} };
        const queryString = JSON.stringify(query);
        const iterator = await ctx.stub.getQueryResult(queryString);
        const allResults = [];
        while (true) {
            const res = await iterator.next();
            if (res.value && res.value.value.toString()) {
                console.log(res.value.value.toString());

                const Key = res.value.key;
                let Record;
                try {
                    Record = JSON.parse(res.value.value.toString());
                } catch (err) {
                    console.log(err);
                    Record = res.value.value.toString();
                }
                allResults.push({ Key, Record });
            }
            if (res.done) {
                console.log('end of data');
                await iterator.close();
                console.info(allResults);
                return JSON.stringify(allResults);
            }
        }
    }

    @Transaction(false)
    public async queryByPriceRange(ctx: Context, 
                    minPrice : number, maxPrice : number,
                    size: number, bookmark?: string): Promise<string> {
        const query = { selector: { price: { $gte: minPrice}} };
        const queryString = JSON.stringify(query);

        const { iterator, metadata } = await ctx.stub.getQueryResultWithPagination(queryString, size, bookmark);
        const allResults = [];
        while (true) {
            const res = await iterator.next();
            if (res.value && res.value.value.toString()) {
                console.log(res.value.value.toString());

                const Key = res.value.key;
                let Record;
                try {
                    Record = JSON.parse(res.value.value.toString());
                } catch (err) {
                    console.log(err);
                    Record = res.value.value.toString();
                }
                allResults.push({ Key, Record });
            }
            if (res.done) {
                console.log('end of data');
                await iterator.close();
                console.info(allResults);
                return JSON.stringify(allResults);
            }
        }
    }

    public async hasRole(ctx: Context, roles: string[]) {
        const clientID = ctx.clientIdentity;
        for (const roleName of roles) {
            if (clientID.assertAttributeValue('role', roleName)) {
                if (clientID.getMSPID() === 'Org1MSP' && clientID.getAttributeValue('role') === 'Goverment') { return true; }
                if (clientID.getMSPID() === 'Org2MSP' && clientID.getAttributeValue('role') === 'Buyer') { return true; }
            }
        }
        return false;
    }
}
